﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.CrossPlatformInput;

public class UniquePlayer : MonoBehaviour {

	public ThirdPersonUserControl TPUC;
	public ThirdPersonCharacter TPC;
	public Climb climb;
	public Inventory inventory;
	private bool ReadyToClimb;
	private int speedTime;
	private bool doubledSpeed;

	// Use this for initialization
	void Start () {
		ReadyToClimb = false;
		climb.enabled = false;
		speedTime = 0;
		doubledSpeed = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (speedTime != 0) {
			speedTime--;
			if(speedTime == 0 && doubledSpeed){
				doubledSpeed = false;
				TPC.ResetSpeed();
			}
		}


		if (ReadyToClimb && CrossPlatformInputManager.GetButtonUp ("Jump")) {
			TPUC.enabled = false;
			TPC.enabled = false;
			climb.enabled = true;
			ReadyToClimb = false;
		}

		if (CrossPlatformInputManager.GetButtonUp ("ScrollLeft")) {
			inventory.selectLeft ();
		}

		if (CrossPlatformInputManager.GetButtonUp ("ScrollRight")) {
			inventory.selectRight ();
		}

		if (CrossPlatformInputManager.GetButtonUp ("Drop")) {
			GameObject t = inventory.DropItem ();
			if(t != null){
				float spawnDistance = 1.5f;
				Vector3 spawnPos = transform.position + transform.forward*spawnDistance*(-1.0f);
				t.transform.position = new Vector3(spawnPos.x, spawnPos.y + 0.5f, spawnPos.z);
				t.SetActive (true);
				TPC.IncrementSpeedMultiplier();
			}
		}

		if (CrossPlatformInputManager.GetButtonUp ("UseItem")) {
			string use = inventory.UseItem ();

			if(use != "invalid"){
				TPC.IncrementSpeedMultiplier();
			}

			if(use == "Speed"){
				TPC.DoubleSpeed();
				doubledSpeed = true;
				speedTime = 300;
			}
		}

	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Climbable") {
			ReadyToClimb = true;
		}

		if (col.gameObject.tag == "Item") {
			TPC.DecrementSpeedMultiplier();
			inventory.AddItem (col.gameObject);
		}
	}

	void OnTriggerExit(Collider col){
		if (col.gameObject.tag == "Climbable" && !climb.enabled) {
			ReadyToClimb = false;
		}
		
		if (col.gameObject.tag == "ClimbTop" && climb.enabled) {
			climb.MoveForward(transform.rotation.eulerAngles.y);
		}
	}

	public void FinishClimbing(){
		ReadyToClimb = false;
		climb.enabled = false;
		TPUC.enabled = true;
		TPC.enabled = true;
	}
}
