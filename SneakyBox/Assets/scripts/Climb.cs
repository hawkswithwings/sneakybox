﻿using UnityEngine;
using System.Collections;

public class Climb : MonoBehaviour {

	private Vector3 m_Move;
	private float finalLoc;
	private bool useX;
	public UniquePlayer player;
	private bool movingForward;
	private Vector3 final;

	// Use this for initialization
	void Start () {
		useX = false;
		movingForward = false;
		m_Move = new Vector3 (0,1,0);
	}

	// Update is called once per frame
	void Update () {

		if (!movingForward) {
			transform.Translate (m_Move * Time.deltaTime * 6);
		}
		//transform.position = Vector3.MoveTowards (transform.position, finalLoc, Time.deltaTime * 3);

		if (movingForward) {
			transform.position = Vector3.MoveTowards (transform.position, final, Time.deltaTime * 6);
			if (useX) {
				if (transform.position.x <= finalLoc) {
					movingForward = false;
					player.FinishClimbing ();
				}
			} else if (!useX) {
				if (transform.position.z <= finalLoc) {
					movingForward = false;
					player.FinishClimbing ();
				}
			}
		}
	}

	public void MoveForward(float temp){

		if ((temp < 2.0f && temp > -2.0f) || 
		    (temp < 362.0f && temp > 358.0f)) { //Facing Right

		}

		else if (temp < 92.0f && temp > 88.0f) { //Facing Down

		}

		else if (temp < 182.0f && temp > 178.0f) { //Facing Left
			useX = false;
			movingForward = true;
			/*m_Move.x = 0;
			m_Move.y = 0;
			m_Move.z = 1;*/
			finalLoc = transform.position.z - 1.0f;
			final = new Vector3(transform.position.x, transform.position.y, finalLoc);
		}

		else if (temp < 272.0f && temp > 268.0f) { //Facing Up
			useX = true;
			movingForward = true;
			/*m_Move.x = -1;
			m_Move.y = 0;
			m_Move.z = 0;*/
			finalLoc = transform.position.x - 1.0f;
			final = new Vector3(finalLoc, transform.position.y, transform.position.z);
		}
	}
}
