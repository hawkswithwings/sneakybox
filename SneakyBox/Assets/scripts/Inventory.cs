﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour {

	private GameObject[] items;
	private int nextItem;
	private int currentItem;
	private int numItems;

	// Use this for initialization
	void Start () {
		items = new GameObject[10];
		currentItem = 0;
		nextItem = 0;
		numItems = 0;
	}
	
	// Update is called once per frame
	void Update () {
	}

	public bool CanAdd(){
		if(numItems != 10)
			return true;
		return false;
	}

	public bool CanDrop(){
		if (numItems != 0)
			return true;
		return false;
	}

	public void selectRight(){
		if (currentItem != 9) {
			currentItem++;
		} 
		else {
			currentItem = 0;
		}

		Debug.Log ("Curently selecting spot " + currentItem);
	}

	public void selectLeft(){
		if (currentItem != 0) {
			currentItem--;
		} 
		else {
			currentItem = 9;
		}

		Debug.Log ("Curently selecting spot " + currentItem);
	}

	public void AddItem(GameObject item){
		if (numItems != 10) {
			items [nextItem] = item;
			item.SetActive (false);
			nextItem++;
			numItems++;

			if (items [nextItem] != null) {
				nextItem = 0;
				while (items[nextItem] != null && nextItem != 10) {
					nextItem++;
				}
			}
		}

		string inv = "Inventory: ";
		for (int i = 0; i < 10; i++) {
			if(items[i] != null)
				inv += items[i].name + ", ";
			else
				inv += "EMPTY, ";
		}
		Debug.Log (inv);
	}

	public string UseItem(){
		if (items [currentItem] != null) {
			if(items[currentItem].name != "Sphere"){
				string re = items[currentItem].name;
				Destroy(items[currentItem]);
				items[currentItem] = null;
				return re;
			}
		}

		return "invalid";
	}

	public GameObject DropItem(){
		if (numItems != 0) {
			GameObject temp;
			nextItem = currentItem;
			temp = items[nextItem];
			items[nextItem] = null;
			numItems--;
			return temp;
		}
		return null;
	}
}
